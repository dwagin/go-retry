package retry

import (
	"time"
)

// Linear creates a new linear backoff using the starting value of
// base and adding base on each failure (1, 2, 3, 4, 5, 6, 7...), up to max.
//
// Once it overflows, the function constantly returns the maximum time.Duration
// for a 64-bit integer.
//
// It panics if the given base is less than or equal to zero.
func Linear(base time.Duration) BackoffFunc {
	if base <= 0 {
		panic("base must be greater than 0")
	}

	acc := base

	return func() time.Duration {
		val := acc

		acc += base
		if acc < val {
			acc = MaxDuration
		}

		return val
	}
}
