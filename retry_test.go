package retry_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"

	"gitlab.com/dwagin/go-retry"
)

type DoSuite struct {
	suite.Suite
}

func TestDo(t *testing.T) {
	suite.Run(t, new(DoSuite))
}

func (v *DoSuite) TestContext() {
	v.Run("Context already canceled", func() {
		require := v.Require()

		ctx, cancel := context.WithCancel(context.Background())

		backoff := retry.Constant(1 * time.Millisecond).WithMaxRetries(5)

		calls := 0
		fn := func(_ context.Context) error {
			calls++
			return retry.RetryableError(errors.New("retry")) // Always return a RetryableError.
		}

		// Here we cancel the Context *before* the call to Do
		cancel()

		err := retry.Do(ctx, backoff, fn)
		require.Error(err)
		require.Equal(0, calls)
	})

	v.Run("Context canceled", func() {
		require := v.Require()

		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Millisecond)
		defer cancel()

		backoff := retry.Constant(100 * time.Millisecond).WithMaxRetries(3)

		calls := 0
		fn := func(_ context.Context) error {
			calls++
			return retry.RetryableError(errors.New("retry")) // Always return a RetryableError.
		}

		err := retry.Do(ctx, backoff, fn)
		require.Error(err)
		require.ErrorIs(err, context.DeadlineExceeded)
		require.Equal(1, calls)
	})
}

func (v *DoSuite) TestExit() {
	v.Run("Exit on non retryable", func() {
		require := v.Require()

		backoff := retry.Constant(1 * time.Millisecond).WithMaxRetries(3)

		calls := 0
		fn := func(_ context.Context) error {
			calls++
			return errors.New("oops") // not retryable
		}

		err := retry.Do(context.Background(), backoff, fn)
		require.Error(err)
		require.Equal(1, calls)
	})

	v.Run("Exit no error", func() {
		require := v.Require()

		backoff := retry.Constant(1 * time.Millisecond).WithMaxRetries(3)

		calls := 0
		fn := func(_ context.Context) error {
			calls++
			return nil // no error
		}

		err := retry.Do(context.Background(), backoff, fn)
		require.NoError(err)
		require.Equal(1, calls)
	})
}

func (v *DoSuite) TestConstantWithMaxRetries() {
	require := v.Require()

	backoff := retry.Constant(1 * time.Millisecond).WithMaxRetries(5)

	calls := 0
	fn := func(_ context.Context) error {
		calls++
		return retry.ErrAgain // Always retry.
	}

	err := retry.Do(context.Background(), backoff, fn)
	require.Error(err)
	require.Equal(6, calls)
}
