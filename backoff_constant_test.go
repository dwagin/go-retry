package retry_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/suite"

	"gitlab.com/dwagin/go-retry"
)

type ConstantBackoffSuite struct {
	suite.Suite
}

func TestConstantBackoff(t *testing.T) {
	suite.Run(t, new(ConstantBackoffSuite))
}

func (v *ConstantBackoffSuite) TestAlone() {
	//nolint:govet // test structure
	testCases := []struct {
		name     string
		base     time.Duration
		tries    uint
		expected []time.Duration
	}{
		{
			name:     "single",
			base:     1 * time.Nanosecond,
			tries:    1,
			expected: []time.Duration{1},
		},
		{
			name:     "many",
			base:     10 * time.Nanosecond,
			tries:    5,
			expected: []time.Duration{10, 10, 10, 10, 10},
		},
	}

	for i := range testCases {
		test := testCases[i]

		v.Run(test.name, func() {
			require := v.Require()

			backoff := retry.Constant(test.base)
			results := make([]time.Duration, 0, test.tries)

			for range test.tries {
				next := backoff()
				if next <= 0 {
					require.Equal(time.Duration(0), next)

					break
				}

				results = append(results, next)
			}

			require.Equal(test.expected, results)
		})
	}
}

func (v *ConstantBackoffSuite) TestPanic() {
	require := v.Require()

	defer func() {
		if err := recover(); err == nil {
			require.Fail("Panic not occurred")
		}
	}()

	_ = retry.Constant(0)
}

func (v *ConstantBackoffSuite) TestWithCappedDuration() {
	v.Run("Alone", func() {
		require := v.Require()

		backoff := retry.Constant(5 * time.Second).WithCappedDuration(3 * time.Second)

		for range 10 {
			next := backoff()
			require.Equal(3*time.Second, next)
		}
	})

	v.Run("Panic", func() {
		require := v.Require()

		defer func() {
			if err := recover(); err == nil {
				require.Fail("Panic not occurred")
			}
		}()

		_ = retry.Constant(250 * time.Millisecond).WithCappedDuration(0)
	})
}

func (v *ConstantBackoffSuite) TestWithTotalDuration() {
	v.Run("Main #1", func() {
		require := v.Require()

		backoff := retry.Constant(100 * time.Millisecond).WithTotalDuration(200 * time.Millisecond)

		next := backoff()
		require.Equal(100*time.Millisecond, next)
		time.Sleep(90 * time.Millisecond)

		next = backoff()
		require.Equal(100*time.Millisecond, next)
		time.Sleep(40 * time.Millisecond)

		next = backoff()
		require.Equal(time.Duration(0), next)
	})

	v.Run("Main #2", func() {
		require := v.Require()

		backoff := retry.Constant(100 * time.Millisecond).WithTotalDuration(200 * time.Millisecond)

		next := backoff()
		require.Equal(100*time.Millisecond, next)
		time.Sleep(90 * time.Millisecond)

		next = backoff()
		require.Equal(100*time.Millisecond, next)
		time.Sleep(110 * time.Millisecond)

		next = backoff()
		require.Equal(time.Duration(0), next)
	})

	v.Run("Panic", func() {
		require := v.Require()

		defer func() {
			if err := recover(); err == nil {
				require.Fail("Panic not occurred")
			}
		}()

		_ = retry.Constant(250 * time.Millisecond).WithTotalDuration(0)
	})
}

func (v *ConstantBackoffSuite) TestWithJitter() {
	v.Run("Alone", func() {
		require := v.Require()

		backoff := retry.Constant(1 * time.Second).WithJitter(250 * time.Millisecond)

		for range 100_000 {
			next := backoff()
			require.GreaterOrEqual(next, 750*time.Millisecond)
			require.LessOrEqual(next, 1250*time.Millisecond)
			require.NotEqual(1*time.Second, next)
		}
	})

	v.Run("Panic #1", func() {
		require := v.Require()

		defer func() {
			if err := recover(); err == nil {
				require.Fail("Panic not occurred")
			}
		}()

		_ = retry.Constant(250 * time.Millisecond).WithJitter(0)
	})

	v.Run("Panic #2", func() {
		require := v.Require()

		defer func() {
			if err := recover(); err == nil {
				require.Fail("Panic not occurred")
			}
		}()

		_ = retry.Constant(250 * time.Millisecond).WithJitter(retry.MaxDuration)
	})

	v.Run("MinDuration", func() {
		require := v.Require()

		backoff := retry.Constant(time.Duration(1)).WithJitter(retry.MaxDuration / 2)

		for range 100_000 {
			next := backoff()
			require.Greater(next, time.Duration(0))
			require.LessOrEqual(next, retry.MaxDuration)
		}
	})

	v.Run("MaxDuration", func() {
		require := v.Require()

		backoff := retry.Constant(retry.MaxDuration).WithJitter(retry.MaxDuration / 2)

		for range 100_000 {
			next := backoff()
			require.Greater(next, time.Duration(0))
			require.LessOrEqual(next, retry.MaxDuration)
		}
	})
}

func (v *ConstantBackoffSuite) TestWithJitterPercent() {
	v.Run("Alone", func() {
		require := v.Require()

		backoff := retry.Constant(1 * time.Second).WithJitterPercent(20)

		for range 100_000 {
			next := backoff()
			require.GreaterOrEqual(next, 800*time.Millisecond)
			require.LessOrEqual(next, 1200*time.Millisecond)
			require.NotEqual(1*time.Second, next)
		}
	})

	v.Run("Panic #1", func() {
		require := v.Require()

		defer func() {
			if err := recover(); err == nil {
				require.Fail("Panic not occurred")
			}
		}()

		_ = retry.Constant(250 * time.Millisecond).WithJitterPercent(0)
	})

	v.Run("Panic #2", func() {
		require := v.Require()

		defer func() {
			if err := recover(); err == nil {
				require.Fail("Panic not occurred")
			}
		}()

		_ = retry.Constant(250 * time.Millisecond).WithJitterPercent(200)
	})

	v.Run("MinDuration", func() {
		require := v.Require()

		backoff := retry.Constant(time.Duration(1)).WithJitterPercent(99)

		for range 100_000 {
			next := backoff()
			require.Greater(next, time.Duration(0))
			require.LessOrEqual(next, retry.MaxDuration)
		}
	})

	v.Run("MaxDuration", func() {
		require := v.Require()

		backoff := retry.Constant(retry.MaxDuration).WithJitterPercent(10)

		for range 100_000 {
			next := backoff()
			require.Greater(next, time.Duration(0))
			require.LessOrEqual(next, retry.MaxDuration)
		}
	})
}

//nolint:funlen // nested tests
func (v *ConstantBackoffSuite) TestWithMaxRetries() {
	v.Run("Alone", func() {
		require := v.Require()

		//nolint:govet // test structure
		testCases := []struct {
			name     string
			base     time.Duration
			max      uint64
			tries    uint64
			expected []time.Duration
		}{
			{
				name:  "single",
				base:  1 * time.Second,
				max:   1,
				tries: 1,
				expected: []time.Duration{
					1 * time.Second,
				},
			},
			{
				name:  "many",
				base:  3 * time.Second,
				max:   7,
				tries: 7,
				expected: []time.Duration{
					3 * time.Second,
					3 * time.Second,
					3 * time.Second,
					3 * time.Second,
					3 * time.Second,
					3 * time.Second,
					3 * time.Second,
				},
			},
			{
				name:  "max",
				base:  3 * time.Second,
				max:   5,
				tries: 7,
				expected: []time.Duration{
					3 * time.Second,
					3 * time.Second,
					3 * time.Second,
					3 * time.Second,
					3 * time.Second,
					0,
					0,
				},
			},
		}

		for i := range testCases {
			test := testCases[i]

			v.Run(test.name, func() {
				backoff := retry.Constant(test.base).WithMaxRetries(test.max)
				results := make([]time.Duration, 0, test.tries)

				for range test.tries {
					next := backoff()
					results = append(results, next)
				}

				require.Equal(test.expected, results)
			})
		}
	})

	v.Run("Panic", func() {
		require := v.Require()

		defer func() {
			if err := recover(); err == nil {
				require.Fail("Panic not occurred")
			}
		}()

		_ = retry.Constant(250 * time.Millisecond).WithMaxRetries(0)
	})

	v.Run("WithMaxRetries", func() {
		require := v.Require()

		backoff := retry.Constant(1 * time.Second).WithMaxRetries(5).WithMaxRetries(11)

		for i := range 100_000 {
			next := backoff()
			if next <= 0 {
				require.Equal(time.Duration(0), next)
				require.Equal(5, i)

				break
			}

			require.Equal(1*time.Second, next)
		}
	})

	v.Run("WithCappedDuration", func() {
		require := v.Require()

		backoff := retry.Constant(1 * time.Second).WithMaxRetries(5).WithCappedDuration(250 * time.Millisecond)

		for i := range 100_000 {
			next := backoff()
			if next <= 0 {
				require.Equal(time.Duration(0), next)
				require.Equal(5, i)

				break
			}

			require.Equal(250*time.Millisecond, next)
		}
	})

	v.Run("WithTotalDuration", func() {
		require := v.Require()

		backoff := retry.Constant(1 * time.Second).WithMaxRetries(5).WithTotalDuration(250 * time.Second)

		for i := range 100_000 {
			next := backoff()
			if next <= 0 {
				require.Equal(time.Duration(0), next)
				require.Equal(5, i)

				break
			}

			require.Equal(1*time.Second, next)
		}
	})

	v.Run("WithJitter", func() {
		require := v.Require()

		backoff := retry.Constant(1 * time.Second).WithMaxRetries(5).WithJitter(250 * time.Millisecond)

		for i := range 100_000 {
			next := backoff()
			if next <= 0 {
				require.Equal(time.Duration(0), next)
				require.Equal(5, i)

				break
			}

			require.LessOrEqual(750*time.Millisecond, next)
			require.GreaterOrEqual(1250*time.Millisecond, next)
		}
	})

	v.Run("WithJitterPercent", func() {
		require := v.Require()

		backoff := retry.Constant(1 * time.Second).WithMaxRetries(5).WithJitterPercent(10)

		for i := range 100_000 {
			next := backoff()
			if next <= 0 {
				require.Equal(time.Duration(0), next)
				require.Equal(5, i)

				break
			}

			require.LessOrEqual(900*time.Millisecond, next)
			require.GreaterOrEqual(1100*time.Millisecond, next)
		}
	})
}

func BenchmarkConstantBackoff(b *testing.B) {
	b.Run("Alone", func(b *testing.B) {
		b.ReportAllocs()

		for range b.N {
			backoff := retry.Constant(1 * time.Nanosecond)

			for range 60 {
				backoff()
			}
		}
	})

	b.Run("WithMaxRetries", func(b *testing.B) {
		b.ReportAllocs()

		for range b.N {
			backoff := retry.Constant(1 * time.Nanosecond).WithMaxRetries(60)

			for range 60 {
				backoff()
			}
		}
	})

	b.Run("WithCappedDuration", func(b *testing.B) {
		b.ReportAllocs()

		for range b.N {
			backoff := retry.Constant(100 * time.Nanosecond).WithCappedDuration(1 * time.Nanosecond)

			for range 60 {
				backoff()
			}
		}
	})

	b.Run("WithTotalDuration", func(b *testing.B) {
		b.ReportAllocs()

		for range b.N {
			backoff := retry.Constant(1 * time.Nanosecond).WithTotalDuration(250 * time.Millisecond)

			for range 60 {
				backoff()
			}
		}
	})

	b.Run("WithJitter", func(b *testing.B) {
		b.ReportAllocs()

		for range b.N {
			backoff := retry.Constant(100 * time.Nanosecond).WithJitter(5 * time.Nanosecond)

			for range 60 {
				backoff()
			}
		}
	})

	b.Run("WithJitterPercent", func(b *testing.B) {
		b.ReportAllocs()

		for range b.N {
			backoff := retry.Constant(100 * time.Nanosecond).WithJitterPercent(10)

			for range 60 {
				backoff()
			}
		}
	})
}
