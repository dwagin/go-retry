package retry

import (
	"time"
)

// Exp2 creates a new exponential backoff by using the coefficient value and
// doubling it for each attempt (1, 2, 4, 8, 16, 32, 64...), up to max.
//
// Once it overflows, the function constantly returns the maximum time.Duration
// for a 64-bit integer.
//
// It panics if the given base is less than or equal to zero.
func Exp2(c time.Duration) BackoffFunc {
	if c <= 0 {
		panic("coefficient must be greater than 0")
	}

	acc := c

	return func() time.Duration {
		val := acc

		acc += acc
		if acc < val {
			acc = MaxDuration
		}

		return val
	}
}
