package retry_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/suite"

	"gitlab.com/dwagin/go-retry"
)

type FibonacciBackoffSuite struct {
	suite.Suite
}

func TestFibonacciBackoff(t *testing.T) {
	suite.Run(t, new(FibonacciBackoffSuite))
}

func (v *FibonacciBackoffSuite) TestAlone() {
	//nolint:govet // test structure
	testCases := []struct {
		name     string
		base     time.Duration
		tries    uint
		expected []time.Duration
	}{
		{
			name:     "single",
			base:     1 * time.Nanosecond,
			tries:    1,
			expected: []time.Duration{1},
		},
		{
			name:     "many #1",
			base:     1 * time.Nanosecond,
			tries:    14,
			expected: []time.Duration{1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610},
		},
		{
			name:     "many #2",
			base:     2 * time.Nanosecond,
			tries:    14,
			expected: []time.Duration{2, 4, 6, 10, 16, 26, 42, 68, 110, 178, 288, 466, 754, 1220},
		},
		{
			name:     "many #3",
			base:     3 * time.Nanosecond,
			tries:    14,
			expected: []time.Duration{3, 6, 9, 15, 24, 39, 63, 102, 165, 267, 432, 699, 1131, 1830},
		},
		{
			name:  "overflow",
			base:  100_000 * time.Hour,
			tries: 10,
			expected: []time.Duration{
				100_000 * time.Hour,
				200_000 * time.Hour,
				300_000 * time.Hour,
				500_000 * time.Hour,
				800_000 * time.Hour,
				1_300_000 * time.Hour,
				2_100_000 * time.Hour,
				retry.MaxDuration,
				retry.MaxDuration,
				retry.MaxDuration,
			},
		},
	}

	for i := range testCases {
		test := testCases[i]

		v.Run(test.name, func() {
			require := v.Require()

			backoff := retry.Fibonacci(test.base)
			results := make([]time.Duration, 0, test.tries)

			for range test.tries {
				next := backoff()
				if next <= 0 {
					require.Equal(time.Duration(0), next)

					break
				}

				results = append(results, next)
			}

			require.Equal(test.expected, results)
		})
	}
}

func (v *FibonacciBackoffSuite) TestPanic() {
	require := v.Require()

	defer func() {
		if err := recover(); err == nil {
			require.Fail("Panic not occurred")
		}
	}()

	_ = retry.Fibonacci(0)
}

func (v *FibonacciBackoffSuite) TestWithMaxRetries() {
	//nolint:govet // test structure
	testCases := []struct {
		name     string
		base     time.Duration
		max      uint64
		tries    uint64
		expected []time.Duration
	}{
		{
			name:  "single",
			base:  1 * time.Second,
			max:   1,
			tries: 1,
			expected: []time.Duration{
				1 * time.Second,
			},
		},
		{
			name:  "many",
			base:  3 * time.Second,
			max:   5,
			tries: 7,
			expected: []time.Duration{
				3 * time.Second,
				6 * time.Second,
				9 * time.Second,
				15 * time.Second,
				24 * time.Second,
				0,
				0,
			},
		},
	}

	for i := range testCases {
		test := testCases[i]

		v.Run(test.name, func() {
			require := v.Require()

			backoff := retry.Fibonacci(test.base).WithMaxRetries(test.max)
			results := make([]time.Duration, 0, test.tries)

			for range test.tries {
				next := backoff()
				results = append(results, next)
			}

			require.Equal(test.expected, results)
		})
	}
}

func BenchmarkFibonacciBackoff(b *testing.B) {
	b.Run("Alone", func(b *testing.B) {
		b.ReportAllocs()

		for range b.N {
			backoff := retry.Fibonacci(1 * time.Nanosecond)

			for range 60 {
				backoff()
			}
		}
	})

	b.Run("WithMaxRetries", func(b *testing.B) {
		b.ReportAllocs()

		for range b.N {
			backoff := retry.Fibonacci(1 * time.Nanosecond).WithMaxRetries(60)

			for range 60 {
				backoff()
			}
		}
	})
}
