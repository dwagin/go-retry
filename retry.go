package retry

import (
	"context"
	"errors"
	"time"
)

// Func is a function passed to retry.
type Func func(ctx context.Context) error

// Do wrap a function with a backoff to retry. The provided context is the same
// context passed to the retryable Func.
func Do(ctx context.Context, backoff BackoffFunc, fn Func) error {
	var err error

	// Return immediately if ctx is canceled
	select {
	case <-ctx.Done():
		return ctx.Err() //nolint:wrapcheck // transparent wrapper
	default:
	}

	for {
		if err = fn(ctx); err == nil {
			return nil
		}

		// Not retryable
		var rErr *retryableError
		if !errors.As(err, &rErr) {
			return err
		}

		next := backoff()
		if next <= 0 {
			return rErr.Unwrap()
		}

		timer := time.NewTimer(next)
		select {
		case <-timer.C:
			continue
		case <-ctx.Done():
			timer.Stop()
			return ctx.Err() //nolint:wrapcheck // transparent wrapper
		}
	}
}
