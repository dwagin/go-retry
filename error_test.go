package retry_test

import (
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"

	"gitlab.com/dwagin/go-retry"
)

type RetryableErrorSuite struct {
	suite.Suite
}

func TestRetryableError(t *testing.T) {
	suite.Run(t, new(RetryableErrorSuite))
}

func (v *RetryableErrorSuite) TestPrefix() {
	require := v.Require()

	err := retry.RetryableError(errors.New("oops"))
	require.Error(err)
	require.ErrorContains(err, "retryable:")
}

func (v *RetryableErrorSuite) TestNil() {
	require := v.Require()

	err := retry.RetryableError(nil)
	require.NoError(err)
}

func (v *RetryableErrorSuite) TestUnwrap() {
	require := v.Require()

	wrapped := errors.New("wrapped")

	err := retry.RetryableError(fmt.Errorf("%w", wrapped))
	require.Error(err)
	require.ErrorIs(err, wrapped)
}
