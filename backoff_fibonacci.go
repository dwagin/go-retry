package retry

import (
	"time"
)

// Fibonacci creates a new Fibonacci backoff using the starting value.
// The wait time is the sum of the previous two wait times on each
// attempt (1, 1, 2, 3, 5, 8, 13...).
//
// Once it overflows, the function constantly returns the maximum time.Duration
// for a 64-bit integer.
//
// It panics if the given base is less than or equal to zero.
func Fibonacci(start time.Duration) BackoffFunc {
	if start <= 0 {
		panic("start must be greater than 0")
	}

	prev := time.Duration(0)
	curr := start

	return func() time.Duration {
		prevR := prev
		currR := curr

		prev = currR

		val := prevR + currR
		if val < currR {
			val = MaxDuration
		}

		curr = val

		return val
	}
}
