package retry

import (
	"time"
)

// Constant creates a new constant backoff using the value base. The wait time
// is the provided constant value.
//
// It panics if the given base is less than or equal to zero.
func Constant(base time.Duration) BackoffFunc {
	if base <= 0 {
		panic("base must be greater than 0")
	}

	return func() time.Duration {
		return base
	}
}
