package retry

import (
	"math/rand"
	"time"
)

const MaxDuration time.Duration = 1<<63 - 1

// BackoffFunc is a backoff expressed as a function.
type BackoffFunc func() time.Duration

// WithMaxRetries executes the backoff function up until the maximum attempts.
//
// It panics if the given max number of attempts is less than or equal to zero.
func (v BackoffFunc) WithMaxRetries(maxRetries uint64) BackoffFunc {
	if maxRetries < 1 {
		panic("max retries must be greater than 0")
	}

	attempt := uint64(0)

	return func() time.Duration {
		if attempt >= maxRetries {
			return 0
		}

		attempt++

		next := v()
		if next <= 0 {
			return 0
		}

		return next
	}
}

// WithCappedDuration sets a maximum on the duration returned from the next
// backoff. This is NOT a total backoff time, but rather a cap on the maximum
// value a backoff can return. Without another middleware, the backoff will
// continue infinitely.
//
// It panics if the given limit is less than or equal to zero.
func (v BackoffFunc) WithCappedDuration(limit time.Duration) BackoffFunc {
	if limit <= 0 {
		panic("limit must be greater than 0")
	}

	return func() time.Duration {
		next := v()
		if next <= 0 {
			return 0
		}

		if next > limit {
			next = limit
		}

		return next
	}
}

// WithTotalDuration sets a maximum on the total amount of time a backoff should
// execute. It's best-effort, and should not be used to guarantee an exact
// amount of time.
//
// It panics if the given total is less than or equal to zero.
func (v BackoffFunc) WithTotalDuration(total time.Duration) BackoffFunc {
	if total <= 0 {
		panic("total must be greater than 0")
	}

	until := time.Now().Add(total)

	return func() time.Duration {
		diff := time.Until(until)
		if diff <= 0 {
			return 0
		}

		next := v()
		if next <= 0 {
			return 0
		}

		if next > diff {
			return 0
		}

		return next
	}
}

// WithJitter adds the specified jitter. jitter can be interpreted as "+/- jitter".
// For example, if jitter were 5 seconds and the backoff returned 20s,
// the value could be between 15 and 25 seconds.
// The value can never be less than or equal to zero.
//
// It panics if the given jitter is less than or equal to zero.
func (v BackoffFunc) WithJitter(jitter time.Duration) BackoffFunc {
	if jitter <= 0 || jitter > MaxDuration/2 {
		panic("invalid jitter value")
	}

	return func() time.Duration {
		next := v()
		if next <= 0 {
			return 0
		}

		next -= jitter
		val := next + time.Duration(rand.Int63n(int64(jitter)*2)) //nolint:gosec // not for crypto purposes

		if val < next {
			return MaxDuration
		}

		if val <= 0 {
			val = 1
		}

		return val
	}
}

// WithJitterPercent wraps a backoff function and adds the specified jitter
// percentage. jitter can be interpreted as "+/- jitter%". For example,
// if jitter were 5 and the backoff returned 20s, the value could be
// between 19 and 21 seconds.
//
// It panics if the given percent is less than 0 or greater than 100.
func (v BackoffFunc) WithJitterPercent(percent uint) BackoffFunc {
	if percent <= 0 || percent > 100 {
		panic("invalid percent value")
	}

	part := float64(percent) / 100.0

	return func() time.Duration {
		next := v()
		if next <= 0 {
			return 0
		}

		jitter := time.Duration(float64(next)*part) + 1

		next -= jitter
		val := next + time.Duration(rand.Int63n(int64(jitter)*2)) //nolint:gosec // not for crypto purposes

		if val < next {
			return MaxDuration
		}

		if val <= 0 {
			val = 1
		}

		return val
	}
}
