package retry_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/suite"

	"gitlab.com/dwagin/go-retry"
)

type Exp2BackoffSuite struct {
	suite.Suite
}

func TestExp2Backoff(t *testing.T) {
	suite.Run(t, new(Exp2BackoffSuite))
}

func (v *Exp2BackoffSuite) TestAlone() {
	//nolint:govet // test structure
	testCases := []struct {
		name     string
		base     time.Duration
		tries    uint
		expected []time.Duration
	}{
		{
			name:     "single",
			base:     1 * time.Nanosecond,
			tries:    1,
			expected: []time.Duration{1},
		},
		{
			name:     "many #1",
			base:     1 * time.Nanosecond,
			tries:    14,
			expected: []time.Duration{1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192},
		},
		{
			name:     "many #2",
			base:     3 * time.Nanosecond,
			tries:    14,
			expected: []time.Duration{3, 6, 12, 24, 48, 96, 192, 384, 768, 1536, 3072, 6144, 12288, 24576},
		},
		{
			name:     "many #3",
			base:     5 * time.Nanosecond,
			tries:    14,
			expected: []time.Duration{5, 10, 20, 40, 80, 160, 320, 640, 1280, 2560, 5120, 10240, 20480, 40960},
		},
		{
			name:     "many #4",
			base:     7 * time.Nanosecond,
			tries:    14,
			expected: []time.Duration{7, 14, 28, 56, 112, 224, 448, 896, 1792, 3584, 7168, 14336, 28672, 57344},
		},
		{
			name:  "overflow",
			base:  100_000 * time.Hour,
			tries: 10,
			expected: []time.Duration{
				100_000 * time.Hour,
				200_000 * time.Hour,
				400_000 * time.Hour,
				800_000 * time.Hour,
				1_600_000 * time.Hour,
				retry.MaxDuration,
				retry.MaxDuration,
				retry.MaxDuration,
				retry.MaxDuration,
				retry.MaxDuration,
			},
		},
	}

	for i := range testCases {
		test := testCases[i]

		v.Run(test.name, func() {
			require := v.Require()

			backoff := retry.Exp2(test.base)
			results := make([]time.Duration, 0, test.tries)

			for range test.tries {
				next := backoff()
				if next <= 0 {
					require.Equal(time.Duration(0), next)

					break
				}

				results = append(results, next)
			}

			require.Equal(test.expected, results)
		})
	}
}

func (v *Exp2BackoffSuite) TestPanic() {
	require := v.Require()

	defer func() {
		if err := recover(); err == nil {
			require.Fail("Panic not occurred")
		}
	}()

	_ = retry.Exp2(0)
}

func (v *Exp2BackoffSuite) TestWithMaxRetries() {
	//nolint:govet // test structure
	testCases := []struct {
		name     string
		base     time.Duration
		max      uint64
		tries    uint64
		expected []time.Duration
	}{
		{
			name:  "single",
			base:  1 * time.Second,
			max:   1,
			tries: 1,
			expected: []time.Duration{
				1 * time.Second,
			},
		},
		{
			name:  "many",
			base:  3 * time.Second,
			max:   5,
			tries: 7,
			expected: []time.Duration{
				3 * time.Second,
				6 * time.Second,
				12 * time.Second,
				24 * time.Second,
				48 * time.Second,
				0,
				0,
			},
		},
	}

	for i := range testCases {
		test := testCases[i]

		v.Run(test.name, func() {
			require := v.Require()

			backoff := retry.Exp2(test.base).WithMaxRetries(test.max)
			results := make([]time.Duration, 0, test.tries)

			for range test.tries {
				next := backoff()
				results = append(results, next)
			}

			require.Equal(test.expected, results)
		})
	}
}

func BenchmarkExp2Backoff(b *testing.B) {
	b.Run("Alone", func(b *testing.B) {
		b.ReportAllocs()

		for range b.N {
			backoff := retry.Exp2(1 * time.Nanosecond)

			for range 60 {
				backoff()
			}
		}
	})

	b.Run("WithMaxRetries", func(b *testing.B) {
		b.ReportAllocs()

		for range b.N {
			backoff := retry.Exp2(1 * time.Nanosecond).WithMaxRetries(60)

			for range 60 {
				backoff()
			}
		}
	})
}
