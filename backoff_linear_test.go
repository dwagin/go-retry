package retry_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/suite"

	"gitlab.com/dwagin/go-retry"
)

type LinearBackoffSuite struct {
	suite.Suite
}

func TestLinearBackoff(t *testing.T) {
	suite.Run(t, new(LinearBackoffSuite))
}

func (v *LinearBackoffSuite) TestAlone() {
	//nolint:govet // test structure
	testCases := []struct {
		name     string
		base     time.Duration
		tries    uint
		expected []time.Duration
	}{
		{
			name:     "single",
			base:     1 * time.Nanosecond,
			tries:    1,
			expected: []time.Duration{1},
		},
		{
			name:     "many",
			base:     1 * time.Nanosecond,
			tries:    14,
			expected: []time.Duration{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14},
		},
		{
			name:  "overflow",
			base:  300_000 * time.Hour,
			tries: 11,
			expected: []time.Duration{
				300_000 * time.Hour,
				600_000 * time.Hour,
				900_000 * time.Hour,
				1_200_000 * time.Hour,
				1_500_000 * time.Hour,
				1_800_000 * time.Hour,
				2_100_000 * time.Hour,
				2_400_000 * time.Hour,
				retry.MaxDuration,
				retry.MaxDuration,
				retry.MaxDuration,
			},
		},
	}

	for i := range testCases {
		test := testCases[i]

		v.Run(test.name, func() {
			require := v.Require()

			backoff := retry.Linear(test.base)
			results := make([]time.Duration, 0, test.tries)

			for range test.tries {
				next := backoff()
				if next <= 0 {
					require.Equal(time.Duration(0), next)

					break
				}

				results = append(results, next)
			}

			require.Equal(test.expected, results)
		})
	}
}

func (v *LinearBackoffSuite) TestPanic() {
	require := v.Require()

	defer func() {
		if err := recover(); err == nil {
			require.Fail("Panic not occurred")
		}
	}()

	_ = retry.Linear(0)
}

func (v *LinearBackoffSuite) TestWithMaxRetries() {
	//nolint:govet // test structure
	testCases := []struct {
		name     string
		base     time.Duration
		max      uint64
		tries    uint64
		expected []time.Duration
	}{
		{
			name:  "single",
			base:  1 * time.Second,
			max:   1,
			tries: 1,
			expected: []time.Duration{
				1 * time.Second,
			},
		},
		{
			name:  "many",
			base:  3 * time.Second,
			max:   5,
			tries: 7,
			expected: []time.Duration{
				3 * time.Second,
				6 * time.Second,
				9 * time.Second,
				12 * time.Second,
				15 * time.Second,
				0,
				0,
			},
		},
	}

	for i := range testCases {
		test := testCases[i]

		v.Run(test.name, func() {
			require := v.Require()

			backoff := retry.Linear(test.base).WithMaxRetries(test.max)
			results := make([]time.Duration, 0, test.tries)

			for range test.tries {
				next := backoff()
				results = append(results, next)
			}

			require.Equal(test.expected, results)
		})
	}
}

func BenchmarkLinearBackoff(b *testing.B) {
	b.Run("Alone", func(b *testing.B) {
		b.ReportAllocs()

		for range b.N {
			backoff := retry.Linear(1 * time.Nanosecond)

			for range 60 {
				backoff()
			}
		}
	})

	b.Run("WithMaxRetries", func(b *testing.B) {
		b.ReportAllocs()

		for range b.N {
			backoff := retry.Linear(1 * time.Nanosecond).WithMaxRetries(60)

			for range 60 {
				backoff()
			}
		}
	})
}
